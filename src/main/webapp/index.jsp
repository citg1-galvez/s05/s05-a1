<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1" import="java.util.Date, java.time.*,java.time.format.DateTimeFormatter"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Java Server Page</title>
</head>
<body>
	<h1>Our Date and Time now is ...</h1>
	


		<%! 			
        	ZonedDateTime manila = ZonedDateTime.now(ZoneId.of("Asia/Manila"));
        	ZonedDateTime japan = ZonedDateTime.now(ZoneId.of("Japan"));
        	ZonedDateTime germany = ZonedDateTime.now(ZoneId.of("Europe/Berlin"));
		%>
        
        
       <%
	       DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
       %>
	
	<p>Manila: <%=manila.format(formatter)  %></p>
	<p>Japan: <%=japan.format(formatter) %></p>
	<p>Germany: <%=germany.format(formatter)%></p>
	
	<%!
		private int initVar = 0;
		private int serviceVar = 0;
		private int destroyVar = 0;
	%>
	
	<%!
		public void jspInit(){
			initVar++;
			System.out.println("jspInit(): init" + initVar);
		}
		public void jspDestroy(){
			destroyVar++;
			System.out.println("jspDestroy(): destroy" + destroyVar);
		}
	%>
	
	<%
		serviceVar++;
		System.out.println("__jspService: service" + serviceVar);
		
		String content1 = "content1 : " + initVar;
		String content2 = "content2 : " + serviceVar;
		String content3 = "content3 : " + destroyVar;
	%>
	
	<h1>JSP Expression</h1>
	<p><%=content1 %>
	<p><%=content2 %>
	<p><%=content3 %>
	
</body>
</html>